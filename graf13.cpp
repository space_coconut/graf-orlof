#include <graf13.h>

template <class T>
Graf<T>::Graf(size_t SizeV){
    SizeVertex=SizeV;
    Vertex=new T[SizeVertex];
    AdjMatrix=new int*[SizeVertex];
    for(size_t i=0; i<SizeVertex; i++)
        AdjMatrix[i]=new int[SizeVertex];
    for(size_t i=0; i<SizeVertex; i++)
        for(size_t j=0; j<SizeVertex; j++)
            AdjMatrix[i][j]=0;
}

template <class T>
void Graf<T>::set_vertex(size_t Index,T Value){
    Vertex[Index-1]=Value;
}

template <class T>
T Graf<T>::get_vertex(size_t Index){
    return Vertex[Index-1];
}

template <class T>
void Graf<T>::show_adj_matrix(){
    for(size_t i=0; i<SizeVertex; i++)
    {
        for(size_t j=0; j<SizeVertex; j++)
            std::cout<< AdjMatrix[i][j] << " ";
        std::cout<< std::endl;
    }
}

template <class T>
void Graf<T>::addConnectionListAdjacency(size_t Index){
    if((Index>0) && (Index<=SizeVertex)){
        char Ch='0';
        std::cout<< "Connection with " << Index << " vertex: ";
        while(Ch!='\n'){
            size_t Index2;
            std::cin>> Index2;
            if((Index2>0) && (Index2<=SizeVertex)){
                AdjMatrix[Index-1][Index2-1]=1;
            }
            Ch=std::cin.get();
            if(Ch=='\n')
                break;
        }
    }
}

template <class T>
size_t Graf<T>::size(){
    return SizeVertex;
}

template <class T>
void Graf<T>::delConnectionListAdjacency(size_t Index){
    if((Index>0) && (Index<=SizeVertex)){
        char Ch='0';
        std::cout<< "Delete connections with " << Index << " vertex: ";
        while(Ch!='\n'){
            size_t Index2;
            std::cin>> Index2;
            if((Index2>0) && (Index2<=SizeVertex)){
                AdjMatrix[Index-1][Index2-1]=0;
            }
            Ch=std::cin.get();
            if(Ch=='\n')
                break;
        }
    }
}

template <class T>
void Graf<T>::clearConnection(){
    for(size_t i=0; i<SizeVertex; i++)
        for(size_t j=0; j<SizeVertex; j++)
            AdjMatrix[i][j]=0;
}

template <class T>
void Graf<T>::del_vertex(size_t Index){
    // удаление вершины
    Index--;
    T* bufferVertex=new T[SizeVertex-1];
    for(size_t i=0; i<Index; i++)
        bufferVertex[i]=Vertex[i];
    for(size_t i=Index+1; i<SizeVertex; i++)
        bufferVertex[i-1]=Vertex[i];
    delete[]Vertex;
    Vertex=new T[SizeVertex-1];
    SizeVertex--;
    for(size_t i=0; i<SizeVertex; i++)
        Vertex[i]=bufferVertex[i];
    delete[]bufferVertex;
    // удаление строки и столбца из матрицы смежности
    int** bufferMatrix=new int*[SizeVertex];
    for(size_t i=0; i<SizeVertex; i++)
        bufferMatrix[i]=new int[SizeVertex];
    for(size_t i=0; i<Index; i++)
        for(size_t j=0; j<Index; j++)
            bufferMatrix[i][j]=AdjMatrix[i][j];
    for(size_t i=Index; i<SizeVertex; i++)
        for(size_t j=0; j<Index; j++)
            bufferMatrix[i][j]=AdjMatrix[i+1][j];
    for(size_t i=0; i<Index; i++)
        for(size_t j=Index; j<SizeVertex; j++)
            bufferMatrix[i][j]=AdjMatrix[i][j+1];
    for(size_t i=Index; i<SizeVertex; i++)
        for(size_t j=Index; j<SizeVertex; j++)
            bufferMatrix[i][j]=AdjMatrix[i+1][j+1];
    for(size_t i=0; i<SizeVertex+1; i++)
        delete[]AdjMatrix[i];
    delete[]AdjMatrix;
    AdjMatrix=new int*[SizeVertex];
    for(size_t i=0; i<SizeVertex; i++)
        AdjMatrix[i]=new int[SizeVertex];
    for(size_t i=0; i<SizeVertex; i++)
        for(size_t j=0; j<SizeVertex;j++)
            AdjMatrix[i][j]=bufferMatrix[i][j];
}

template <class T>
void Graf<T>::directed_to_undirected(){
    for(size_t i=0; i<SizeVertex; i++)
        for(size_t j=0; j<SizeVertex; j++)
            if(AdjMatrix[i][j]==0)
                AdjMatrix[i][j]=AdjMatrix[j][i];
}

template <class T>
void Graf<T>::degryVertex(size_t Index){
    size_t Count=0;
    for(size_t i=0; i<SizeVertex; i++)
        if(AdjMatrix[Index-1][i]>0)
            Count++;
    std::cout<< "Number of connection with " << Index << ": "<< Count << std::endl;
}



