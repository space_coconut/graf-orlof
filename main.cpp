#include <iostream>
#include <graf13.h>
#include <graf13.cpp>
#include <ctime>


int main(){
    srand(time(NULL));

    Graf<int> A(5);

    std::cout<< "Vertex of graph:" << std::endl;
    for(size_t i=1; i<A.size()+1; i++)
    {
        A.set_vertex(i, rand()%10);
        std::cout<< A.get_vertex(i) << " ";
    }

    std::cout<< std::endl;
    for(size_t i=1; i<A.size()+1; i++)
        A.addConnectionListAdjacency(i);

    std::cout<< std::endl << "Adjacency matrix of graph:" << std::endl;

    A.show_adj_matrix();

    std::cout<< std::endl;

    for(size_t i=1; i<A.size()+1; i++)
          A.degryVertex(i);

    std::cout<< std::endl << std::endl;

    for(size_t i=1; i<A.size()+1; i++)
        A.delConnectionListAdjacency(i);

    std::cout<< std::endl << "New adjacency matrix of graph:" << std::endl;

    A.show_adj_matrix();

    std::cout<< std::endl;

    size_t v;
    std::cout<< "Delete vertex ";
    std::cin>> v;

    A.del_vertex(v);

    A.show_adj_matrix();

    std::cout<< std::endl << "Directed to undirected:" << std::endl;

    A.directed_to_undirected();

    A.show_adj_matrix();

    std::cout<< std::endl;

    A.clearConnection();

    std::cout<< "Connections cleared!" << std::endl;

    A.show_adj_matrix();

    return 0;
}
