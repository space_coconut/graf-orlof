#ifndef GRAF13_H
#define GRAF13_H

#include <iostream>

template <class T>
class Graf{
    size_t SizeVertex;
    T* Vertex;
    int** AdjMatrix;
public:
    Graf(size_t SizeV);

    void set_vertex(size_t Index, T Value); //задание вершин

    T get_vertex(size_t Index); //получение вершин

    void show_adj_matrix(); //вывод матрицы смежности

    void addConnectionListAdjacency(size_t Index); //добавление связей

    size_t size(); //получение размера

    void delConnectionListAdjacency(size_t Index); //удаление связей

    void clearConnection(); //удаление ВСЕХ связей

    void del_vertex(size_t Index); //удаление вершины

    void directed_to_undirected(); //симметретизация

    void degryVertex(size_t Index); //степень вершины
};

#endif // GRAF13_H
